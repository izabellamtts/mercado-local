import isHttp from 'is-http'

export default (img, isBackground, blend) => {
  const backgroundOverlay = blend ? 'rgba(' + blend + ')' : ''
  let imagem
  if (!img) return null
  const isRemote = isHttp(img)
  if (isRemote) {
    imagem = img
  } else {
    try {
      imagem = require(`~/static/${img}`)
    } catch (e) {
      imagem = 'bg-balaio.jpg'
    }
  }
  if (isBackground) return `background: url("${imagem}") ${backgroundOverlay};`
  else return imagem
}
