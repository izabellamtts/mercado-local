export default {
  theme: {
    extend: {
      colors: {
        light: '#ebebeb',
        secondary: '#FDAA03',
        primary: '#8d6d54',
        fundo: '#fefef9',
      },
    },
  },
  shortcuts: {
    icone: 'w-100px md:w-150px cursor-pointer',
    divicone:
      'w-140px h-55px flex justify-center items-center hover:bg-red-600 bg-opacity-95',
    picone: 'text-lg text-white text-shadow',
    menuitem:
      'block text-sm px-2 py-4 text-white bg-gray-600 font-semibold w-full',
    nav: 'w-full grid lg:grid-cols-3 lg:gap-x-5 justify-around place-items-center place-content-center gap-y-50px lg:gap-y-0',
    navtitle: 'ml-4 text-center text-2xl uppercase font-dark pb-12',
    navitem:
      'w-60vw grid lg:w-200px lg:grid-rows-2 place-items-center place-content-center lg:place-content-start lg:place-items-auto',
  },
}
