require('dotenv').config()

export default {
  head: {
    link: [
      {
        rel: 'stylesheet',
        href: '/mapbox.css',
      },
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' },
    ],
    title: 'PSP',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'PSP',
        name: 'PSP',
        content:
          'Balaio comercio de produtos organicos certificados com modelo mais justo para produtores e consumidores',
      },
    ],
  },
  env: {
    tileServer: process.env.TILE_SERVER,
  },
  css: ['node_modules/lite-youtube-embed/src/lite-yt-embed.css'],
  plugins: [{ src: '~/plugins/mapbox.client' }, '~/plugins/youtube.client.js'],
  modules: ['@nuxt/content', 'nuxt-i18n', '@nuxt/http'],
  i18n: {},
  googleFonts: {
    download: true,
    base64: true,
    inject: true,
    families: {
      Montserrat: true,
      'Montserrat+Alternates': true,
    },
  },
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module',
    // https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    // https://google-fonts.nuxtjs.org/
    '@nuxtjs/google-fonts',
    'nuxt-windicss',
    '@nuxtjs/pwa',
    '@nuxtjs/localforage',
    '@nuxtjs/svg',
  ],
  components: true,
  build: {
    extend(config, ctx) {}, // blah blah
  },
  server: {
    host: '0.0.0.0',
  },
}
