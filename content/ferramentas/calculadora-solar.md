---
nome: Calculadora Solar
descricao: Calcular energia solar
icone: ferramentas/solar.gif
imagem: ferramentas/solar.gif
ativo: sim
destaque:
---

# Calculadora Solar

Sobre o projeto [Calculadora Solar](https://solar.coolab.org) e como a Coolab contribui.
