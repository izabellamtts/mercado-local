---
nome: Jurubeba
descricao: Mesh usando LoRa
icone: ferramentas/jurubeba.png
imagem: ferramentas/jurubeba.png
ativo: sim
destaque: sim
---

# Jurubeba

Sobre o projeto [Jurubeba](https://jurubeba.coolab.org/) e como a Coolab contribui.
