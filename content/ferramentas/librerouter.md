---
nome: Libre Router
descricao: Roteador open hardware
icone: ferramentas/librerouter.png
imagem: ferramentas/librerouter.png
ativo: sim
destaque: sim
---

# Libre Router

Sobre o projeto [Libre Router](https://librerouter.org) e como a Coolab contribui.
