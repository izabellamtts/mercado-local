---
nome: Meshtastic
descricao: Mesh usando LoRa
icone: ferramentas/meshtastic.svg
imagem: ferramentas/meshtastic.svg
ativo: sim
destaque: sim
---

# Meshtastic

Sobre o projeto [Meshtastic](https://meshtastic.org) e como a Coolab contribui.
